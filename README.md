# helm-airgap

## Download image and chart

Download images and the chart helm

        ./helm-airgap.sh --helm_url_repo https://argoproj.github.io/argo-helm --helm_repo argo --version 3.26.3 --helm_chart argo-cd

To identity each parameter, take information give by the installation guide.

ie : https://artifacthub.io/packages/helm/argo/argo-cd?modal=install

Script chart-load-images.sh and chart-save-images.sh are base on rancher script https://github.com/rancher/rancher/releases/tag/v2.6.2
## Upload Images

        ./chart-load-images.sh -l {helm_chart}-images.txt -r {fqdn_private_registry} -i {helm_chart}-images.tar.gz
## BUG
If a chart use container in the system, the container are not take in the package

Exemple : 

https://artifacthub.io/packages/helm/chaos-mesh/chaos-mesh

The container image prom/prometheus:v2.15.2 and gcr.io/google-containers/pause:latest are not take by the script. 
The script search in the charts {images: xxx}. 