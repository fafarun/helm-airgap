#!/bin/bash


usage () {
    echo "USAGE: $0 [--helm_url_repo https://argoproj.github.io/argo-helm] [--helm_repo argo] [--version 3.26.3] [--helm_chart argo-cd]"
    echo "  [-hu|--helm_url_repo path] the url of the public repo helm"
    echo "  [-hr|--helm_repo] project helm you want user/helm_chart"
    echo "  [-v|--version version] version of the helm chart"
    echo "  [-hc|--helm_chart] the name of the chart from helm_repo"
    echo "  [-di|--docker_image] OPTIONAL :  the name of the file to add some docker images. Each should have <IMAGE_DOCKER_URL>:<VERSION>  busybox:latest"
    echo "  [-hv|--helm_version] OPTIONAL : if you want a specific version of helm in your package"
}

POSITIONAL=()
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -hu|--helm_url_repo)
        helm_url_repo="$2"
        shift # past argument
        shift # past value
        ;;
        -hr|--helm_repo)
        helm_repo="$2"
        shift # past argument
        shift # past value
        ;;
        -v|--version)
        version="$2"
        shift # past argument
        shift # past value
        ;;
        -hc|--helm_chart)
        helm_chart="$2"
        shift # past argument
        shift # past value
        ;;
        -di|--docker_image)
        docker_image_file="$2"
        shift # past argument
        shift # past value
        ;;
        -hv|--helm_version)
        version_helm="$2"
        shift # past argument
        shift # past value
        ;;
        -h|--help)
        help="true"
        shift
        ;;
        *)
        usage
        exit 1
        ;;
    esac
done

if [[ $help ]]; then
    usage
    exit 0
fi


if ! command -v helm &> /dev/null
then
    echo "helm could not be found. You have to install it https://helm.sh"
    exit
fi
echo " $(date) : Start to download the helm package" >> helm_chart.log
echo "$0 helm url -hu $helm_url_repo -hr $helm_repo  -v $version  -hc $helm_chart -di $docker_image_file -hv $version_helm" >> helm_chart.log

if [[ -z "$version_helm" ]]; then
    version_helm="v3.8.0"
    echo "get the default helm version $version_helm for Linux"
fi


folder=$helm_chart

echo "Create the destination folder"
mkdir -p $folder
echo "ADD repo to local"
helm repo add $helm_repo  $helm_url_repo
echo "repo update"
helm repo update
echo "pull the $helm_chart in $version in the $folder"
helm pull $helm_repo/$helm_chart --version $version  -d $folder
echo "extract from  $folder/$helm_chart-$version.tgz to generate a tempory folder"
helm template $helm_chart $folder/$helm_chart-$version.tgz --output-dir $folder

echo "found all images and put in the file $folder/$helm_chart-$version-images.txt "
grep -r "image:" $folder/$helm_chart | awk -F" " '{print $3}' | sed 's/"//g' > $folder/$helm_chart-$version-images.txt

echo " add docker image from $docker_image_file in $folder/$helm_chart-$version-images.txt "
if [[ -n "$docker_image_file" ]]; then
    cat $docker_image_file >> $folder/$helm_chart-$version-images.txt
fi

echo "save images in the archive $folder/$helm_chart-$version-images.tar.gz"
./chart-save-images.sh -l $folder/$helm_chart-$version-images.txt -i $folder/$helm_chart-$version-images.tar.gz

echo "md5 files"
md5 $folder/$helm_chart-$version.tgz > $folder/md5.txt 
md5 $folder/$helm_chart-$version-images.tar.gz >> $folder/md5.txt 

echo "Download helm version $version_helm for Linux"
curl -LO https://get.helm.sh/helm-$version_helm-linux-amd64.tar.gz
echo "copy helm version in the $folder"
mv helm-$version_helm-linux-amd64.tar.gz $folder/


echo "copy script to import docker image"
cp chart-load-images.sh $folder/

echo "delete tempory file $folder/$helm_chart"
rm -rf $folder/$helm_chart
helm repo remove $helm_repo

echo "you can download the folder $folder/$helm_chart and put it on the system in airgap mode.  "
echo " you have to use chart-load-images.sh to load in the registry."
echo "you have to use helm template to extract the helm project with your variable"
echo " $(date) : End to download the helm package" >> helm_chart.log
